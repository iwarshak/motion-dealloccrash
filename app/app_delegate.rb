
class AppDelegate
  def application(application, didFinishLaunchingWithOptions:launchOptions)

    AFMotion::Client.build_shared('http://slowload.heroku.com/') do
    end


    @window = UIWindow.alloc.initWithFrame(UIScreen.mainScreen.bounds)
    # Adding Motion-Xray's UIWindow shim
    #@window = Motion::Xray::XrayWindow.alloc.initWithFrame(UIScreen.mainScreen.bounds)
    myNavController = RootController.alloc.init

    @window.rootViewController = UINavigationController.alloc.initWithRootViewController(myNavController)
    # @window.rootViewController.wantsFullScreenLayout = true
    # #Splash Screen
    # image_view = UIImageView.alloc.initWithImage('Default'.uiimage) # automatically grabs correct image for phone
    # @window.rootViewController.view << image_view
    # @window.rootViewController.view.bringSubviewToFront(image_view)
    # normal
    @window.makeKeyAndVisible


    # include the SaveUIPlugin, which is not included by default
    #Motion::Xray.register(Motion::Xray::SaveUIPlugin.new)

    true
  end
end
