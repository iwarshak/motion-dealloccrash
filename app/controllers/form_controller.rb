class FormController < UIViewController
  attr_accessor :delegate

  def viewDidLoad
    @current_operations = NSMutableSet.setWithCapacity(5)

    but = UIButton.buttonWithType(UIButtonTypeRoundedRect)
    but.setTitle('crash', forState:UIControlStateNormal)
    but.frame = [[100,100], [100,100]]
    but.addTarget(self, action:'crash', forControlEvents:UIControlEventTouchUpInside)
    self.view.addSubview(but)
  end


  def crash
    op = AFMotion::Client.shared.get('/5') do |result|
      $r = result
      puts "boom #{result}"
      puts "self is #{self}"
      if result.success?
        self.say
        puts "self.navigationController is #{self.navigationController}"
        self.navigationController.popViewControllerAnimated(true) if self.navigationController
      end
    end
    @current_operations.addObject(op)

  end

  def say
    puts "say!"
  end


  def dealloc
    puts "deallocing form #{self}"
    @current_operations.each do |op|
      puts "hmm"
      # Log.debug("op is #{op}")
      # Log.debug("op #{op} isExecuting:#{op.isExecuting}")
      # Log.debug("cancelling #{op} - #{op.inspect} ")
      # op.setCompletionBlockWithSuccess(nil,failure:nil) # https://github.com/AFNetworking/AFNetworking/pull/693#issuecomment-19837221
      # op.cancel
    end
    super
  end

end
